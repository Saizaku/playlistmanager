/*
 * This file is generated by jOOQ.
 */
package com.street_light.classroom_manager.generated.jooq;


import com.street_light.classroom_manager.generated.jooq.tables.Artist;
import com.street_light.classroom_manager.generated.jooq.tables.Playlist;
import com.street_light.classroom_manager.generated.jooq.tables.Song;
import com.street_light.classroom_manager.generated.jooq.tables.SongList;
import com.street_light.classroom_manager.generated.jooq.tables.User;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in public
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>public.artist</code>.
     */
    public static final Artist ARTIST = com.street_light.classroom_manager.generated.jooq.tables.Artist.ARTIST;

    /**
     * The table <code>public.playlist</code>.
     */
    public static final Playlist PLAYLIST = com.street_light.classroom_manager.generated.jooq.tables.Playlist.PLAYLIST;

    /**
     * The table <code>public.song</code>.
     */
    public static final Song SONG = com.street_light.classroom_manager.generated.jooq.tables.Song.SONG;

    /**
     * The table <code>public.song_list</code>.
     */
    public static final SongList SONG_LIST = com.street_light.classroom_manager.generated.jooq.tables.SongList.SONG_LIST;

    /**
     * The table <code>public.user</code>.
     */
    public static final User USER = com.street_light.classroom_manager.generated.jooq.tables.User.USER;
}
