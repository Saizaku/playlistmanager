/*
 * This file is generated by jOOQ.
 */
package com.street_light.classroom_manager.generated.jooq.tables.daos;


import com.street_light.classroom_manager.generated.jooq.tables.Artist;
import com.street_light.classroom_manager.generated.jooq.tables.records.ArtistRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ArtistDao extends DAOImpl<ArtistRecord, com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist, Integer> {

    /**
     * Create a new ArtistDao without any configuration
     */
    public ArtistDao() {
        super(Artist.ARTIST, com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist.class);
    }

    /**
     * Create a new ArtistDao with an attached configuration
     */
    public ArtistDao(Configuration configuration) {
        super(Artist.ARTIST, com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist> fetchById(Integer... values) {
        return fetch(Artist.ARTIST.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist fetchOneById(Integer value) {
        return fetchOne(Artist.ARTIST.ID, value);
    }

    /**
     * Fetch records that have <code>name IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist> fetchByName(String... values) {
        return fetch(Artist.ARTIST.NAME, values);
    }
}
