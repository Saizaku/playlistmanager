package com.street_light.classroom_manager.artist;

import com.google.gson.Gson;
import com.street_light.classroom_manager.artist.forms.ArtistForm;
import com.street_light.classroom_manager.auth.forms.ResponseForm;
import com.street_light.classroom_manager.generated.jooq.tables.daos.ArtistDao;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.Artist;
import io.vertx.ext.web.RoutingContext;

import javax.xml.ws.Response;
import java.util.List;

public class ArtistController {
    ArtistDao artistDao;
    Gson gson;

    public ArtistController(ArtistDao artistDao, Gson gson) {
        this.artistDao = artistDao;
        this.gson = gson;
    }

    public void insertArtist(RoutingContext ctx){
        ArtistForm artistForm = gson.fromJson(ctx.getBodyAsString(),ArtistForm.class);

        Artist artist = new Artist();
        artist.setName(artistForm.getName());

        artistDao.insert(artist);

        ResponseForm res = new ResponseForm(true,"ok",0);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(res));
    }

    public void getArtist(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("id"));

        Artist artist = artistDao.fetchOneById(id);

        if(artist == null){
            ResponseForm res = new ResponseForm(false,"worng id",0);

            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }
        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(artist));
    }

    public void getArtistByName(RoutingContext ctx){
        String name = ctx.request().getParam("name");

        List<Artist> artists = artistDao.fetchByName(name);

        if(artists.isEmpty()){
            ResponseForm res = new ResponseForm(false,"Empty",0);

            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(artists.get(0));
    }

}
