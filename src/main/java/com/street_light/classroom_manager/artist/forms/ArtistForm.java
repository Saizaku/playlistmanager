package com.street_light.classroom_manager.artist.forms;

public class ArtistForm {
    String name;

    public ArtistForm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
