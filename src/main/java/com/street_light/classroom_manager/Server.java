package com.street_light.classroom_manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.street_light.classroom_manager.artist.ArtistController;
import com.street_light.classroom_manager.auth.AuthController;
import com.street_light.classroom_manager.file.FileController;
import com.street_light.classroom_manager.generated.jooq.tables.daos.*;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.Message;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.User;
import com.street_light.classroom_manager.group.GroupController;
import com.street_light.classroom_manager.playlist.PlaylistController;
import com.street_light.classroom_manager.song.SongController;
import com.zaxxer.hikari.HikariDataSource;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.BridgeOptions;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import org.flywaydb.core.Flyway;
import org.jooq.impl.DefaultConfiguration;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;


public class Server extends AbstractVerticle {

    private Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
            .create();

    private HikariDataSource dataSource = new HikariDataSource();

    private Flyway flyway = new Flyway();

    private Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2i);

    @Override
    public void start(Future<Void> fut)  {



        dataSource.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        dataSource.addDataSourceProperty("serverName", "localhost");
        dataSource.addDataSourceProperty("databaseName", "classroom_manager");
        dataSource.addDataSourceProperty("user", "postgres");
        dataSource.addDataSourceProperty("password", "local");


        flyway.setDataSource(dataSource);
        flyway.baseline();
        flyway.migrate();


        System.out.println(argon2.hash(32, 16384, 1, "password"));

        UserDao userDao = new UserDao(new DefaultConfiguration()
                .set(dataSource));
        SongDao songDao = new SongDao(new DefaultConfiguration()
                .set(dataSource));
        ArtistDao artistDao = new ArtistDao(new DefaultConfiguration()
                .set(dataSource));
        PlaylistDao playlistDao = new PlaylistDao(new DefaultConfiguration()
                .set(dataSource));
        SongListDao songListDao = new SongListDao(new DefaultConfiguration()
                .set(dataSource));

        AuthController authController = new AuthController(userDao, argon2, vertx, gson);
        SongController songController = new SongController(songDao, gson);
        ArtistController artistController = new ArtistController(artistDao, gson);
        PlaylistController playlistController = new PlaylistController(playlistDao, songListDao, gson);

        Router router = Router.router(vertx);

        router.route("/api/*").handler(JWTAuthHandler.create(authController.getJwt()));
        router.route("/api/*").handler(BodyHandler.create());
        router.route("/auth/*").handler(BodyHandler.create());
        router.post("/auth/login").handler(authController::LogIn);
        router.post("/auth/register").handler(authController::Register);
        //bind routes for song
        router.post("/api/insertSong").handler(songController::insertSong);
        router.get("/api/getSong/:id").handler(songController::getSong);
        router.get("/api/getSongByName/:name").handler(songController::getSongByName);
        //bind routes for artist
        router.post("/api/insertArtist").handler(artistController::insertArtist);
        router.get("/api/getArtist/:id").handler(artistController::getArtist);
        router.get("/api/getArtistByName/:name").handler(artistController::getArtistByName);
        //bind routes for playlist
        router.post("/api/insertPlaylist").handler(playlistController::createPlaylist);
        router.post("/api/addSongToPlaylist").handler(playlistController::addSongToPlaylist);
        router.get("/api/getPlaylist/:id").handler(playlistController::getPlaylist);
        router.get("/api/getUserPlaylists/:id").handler(playlistController::getPlaylistByUser);

        HttpServer httpServer = vertx
                .createHttpServer()
                .requestHandler(router::accept);

        BridgeOptions opts = new BridgeOptions()
                .addInboundPermitted(new PermittedOptions().setAddress("chat.to.server"))
                .addOutboundPermitted(new PermittedOptions().setAddress("chat.to.client"));

        SockJSHandler ebHandler = SockJSHandler.create(vertx);
        router.route("/eventbus/*").handler(ebHandler);

        EventBus eb = vertx.eventBus();

        httpServer.listen(4567);
    }
}