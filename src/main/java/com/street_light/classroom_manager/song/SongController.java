package com.street_light.classroom_manager.song;

import com.google.gson.Gson;
import com.street_light.classroom_manager.auth.forms.ResponseForm;
import com.street_light.classroom_manager.generated.jooq.tables.daos.SongDao;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.Song;
import io.vertx.ext.web.RoutingContext;
import com.street_light.classroom_manager.song.forms.SongForm;

import java.util.List;

public class SongController {
    SongDao songDao;
    Gson gson;

    public SongController(SongDao songDao, Gson gson) {
        this.songDao = songDao;
        this.gson = gson;
    }

    public void insertSong(RoutingContext ctx){
        SongForm songForm = gson.fromJson(ctx.getBodyAsString(), SongForm.class);

        Song song = new Song();
        song.setName(songForm.getName());
        song.setIdArtist(songForm.getId_artist());
        song.setYtLink(songForm.getYt_link());

        songDao.insert(song);

        ResponseForm res = new ResponseForm(true,"ok",0);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(res));

    }

    public void getSong(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("id"));

        Song song = songDao.fetchOneById(id);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(song));
    }

    public void getSongByName(RoutingContext ctx){
        String name = ctx.request().getParam("name");

        List<Song> songs = songDao.fetchByName(name);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(songs));
    }
}
