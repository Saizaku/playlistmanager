package com.street_light.classroom_manager.song.forms;

public class SongForm {
    String name;
    int id_artist;
    String yt_link;

    public SongForm(String name, int id_artist, String yt_link) {
        this.name = name;
        this.id_artist = id_artist;
        this.yt_link = yt_link;
    }

    public String getYt_link() {
        return yt_link;
    }

    public void setYt_link(String yt_link) {
        this.yt_link = yt_link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_artist() {
        return id_artist;
    }

    public void setId_artist(int id_artist) {
        this.id_artist = id_artist;
    }
}
