package com.street_light.classroom_manager.playlist;

import com.google.gson.Gson;
import com.street_light.classroom_manager.auth.forms.ResponseForm;
import com.street_light.classroom_manager.generated.jooq.tables.daos.PlaylistDao;
import com.street_light.classroom_manager.generated.jooq.tables.daos.SongListDao;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.Playlist;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.SongList;
import com.street_light.classroom_manager.playlist.forms.PlaylistForm;
import com.street_light.classroom_manager.playlist.forms.SongListForm;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

public class PlaylistController {
    PlaylistDao playlistDao;
    SongListDao songlistDao;
    Gson gson;

    public PlaylistController(PlaylistDao playlistDao, SongListDao songlistDao, Gson gson) {
        this.playlistDao = playlistDao;
        this.songlistDao = songlistDao;
        this.gson = gson;
    }

    public void createPlaylist(RoutingContext ctx){
        PlaylistForm playlistForm = gson.fromJson(ctx.getBodyAsString(),PlaylistForm.class);

        Playlist playlist = new Playlist();

        playlistDao.insert(playlist);

        ResponseForm res = new ResponseForm(true,"ok",0);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(res));
    }

    public void getPlaylist(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("id"));

        Playlist playlist = playlistDao.fetchOneById(id);

        if(playlist == null){
            ResponseForm res = new ResponseForm(false,"worng id",0);

            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(playlist));
    }

    public void getPlaylistByUser(RoutingContext ctx){
        int user_id = Integer.parseInt(ctx.request().getParam("id"));

        List<Playlist> playlists = playlistDao.fetchByIdUser(user_id);

        if(playlists.isEmpty()){
            ResponseForm res = new ResponseForm(false,"worng id",0);

            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(playlists));
    }

    public void addSongToPlaylist(RoutingContext ctx){
        SongListForm songListForm = gson.fromJson(ctx.getBodyAsString(), SongListForm.class);

        SongList songList = new SongList();
        songList.setIdPlaylist(songListForm.getId_palylist());
        songList.setIdSong(songListForm.getId_song());
        songlistDao.insert(songList);

        ResponseForm res = new ResponseForm(true,"ok",0);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(res));
    }
}
