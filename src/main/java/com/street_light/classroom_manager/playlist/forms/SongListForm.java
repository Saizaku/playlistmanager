package com.street_light.classroom_manager.playlist.forms;

public class SongListForm {
    int id_song;
    int id_palylist;

    public SongListForm(int id_song, int id_palylist) {
        this.id_song = id_song;
        this.id_palylist = id_palylist;
    }

    public int getId_song() {
        return id_song;
    }

    public void setId_song(int id_song) {
        this.id_song = id_song;
    }

    public int getId_palylist() {
        return id_palylist;
    }

    public void setId_palylist(int id_palylist) {
        this.id_palylist = id_palylist;
    }
}
