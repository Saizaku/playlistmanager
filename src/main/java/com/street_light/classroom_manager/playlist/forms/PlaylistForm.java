package com.street_light.classroom_manager.playlist.forms;

public class PlaylistForm {
    String name;
    int id_user;

    public PlaylistForm(String name, int id_user) {
        this.name = name;
        this.id_user = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
}
