package com.street_light.classroom_manager.auth.forms;

public class ResponseForm {
    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    boolean success;
    String message;
    int id;

    public ResponseForm(boolean success, String message, int id) {
        this.success = success;
        this.message = message;
        this.id = id;
    }
}
