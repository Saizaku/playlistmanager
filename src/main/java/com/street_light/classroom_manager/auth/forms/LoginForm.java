package com.street_light.classroom_manager.auth.forms;

public class LoginForm {
    String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    String password;

    public LoginForm(String email, String password){
        this.email = email;
        this.password = password;
    }


}
