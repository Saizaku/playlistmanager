package com.street_light.classroom_manager.auth;

import com.google.gson.Gson;
import com.street_light.classroom_manager.auth.forms.LoginForm;
import com.street_light.classroom_manager.auth.forms.RegisterForm;
import com.street_light.classroom_manager.auth.forms.ResponseForm;
import com.street_light.classroom_manager.generated.jooq.tables.daos.UserDao;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.User;
import de.mkammerer.argon2.Argon2;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.RoutingContext;

import javax.xml.ws.Response;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

public class AuthController {
    UserDao userDao;
    Argon2 argon2;
    Gson gson;

    // Hash parameters, aimed at 500ms
    private static int HASH_ITERATIONS = 32;        // 2^5 rounds
    private static int HASH_MEMORY = 16384;         // 16 MiB (2^14 bytes)
    private static int HASH_PARALLELISM = 1;        // 1 thread

    JWTAuth jwt;

    public AuthController(UserDao userDao, Argon2 argon2, Vertx vertx, Gson gson){
        this.userDao = userDao;
        this.argon2 = argon2;
        this.gson = gson;
        JWTAuthOptions config = new JWTAuthOptions(new JsonObject()
                .put("keyStore", new JsonObject()
                        .put("type", "jceks")
                        .put("path", "keystore.jceks")
                        .put("password", "HJM5s4QxGKyCkxVC")));

        jwt = JWTAuth.create(vertx, config);
    }

    public JWTAuth getJwt() {
        return jwt;
    }

    public void LogIn(RoutingContext ctx){
        LoginForm loginForm = gson.fromJson(ctx.getBodyAsString(), LoginForm.class);

        List<User> users = userDao.fetchByEmail(loginForm.getEmail());

        ResponseForm res = new ResponseForm(false, "",-1);

        if(users.isEmpty()) {
            res.setSuccess(false);
            res.setMessage("Not found");
            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }

        if(!argon2.verify(users.get(0).getPassword(), loginForm.getPassword())){
            res.setSuccess(false);
            res.setMessage("Wrong username or password");
            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }

        res.setSuccess(true);
        res.setMessage(jwt.generateToken(new JsonObject()));
        res.setId(users.get(0).getId());

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .setStatusMessage("OK")
                .end(gson.toJson(res));
    }

    public void Register(RoutingContext ctx){
        RegisterForm registerForm = gson.fromJson(ctx.getBodyAsString(), RegisterForm.class);

        ResponseForm res = new ResponseForm(false, "",-1);

        List<User> users = userDao.fetchByEmail(registerForm.getEmail());
        if(!users.isEmpty()) {
            res.setSuccess(false);
            res.setMessage("A user with that email already exists");

            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }

        users = userDao.fetchByUsername(registerForm.getUsername());
        if(!users.isEmpty()){
            res.setSuccess(false);
            res.setMessage("Username taken");

            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(res));

            return;
        }

        User user = new User();
        user.setEmail(registerForm.getEmail());
        user.setUsername(registerForm.getUsername());
        user.setPassword(argon2.hash(HASH_ITERATIONS, HASH_MEMORY, HASH_PARALLELISM, registerForm.getPassword()))
        userDao.insert(user);

        res.setSuccess(true);
        res.setMessage(jwt.generateToken(new JsonObject()));

        users = userDao.fetchByUsername(registerForm.getUsername());

        res.setId(users.get(0).getId());

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(res));
    }


}
