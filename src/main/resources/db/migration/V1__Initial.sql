CREATE TABLE public."user"
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    username character varying(128) COLLATE pg_catalog."default",
    email character varying(128) COLLATE pg_catalog."default",
    password character varying(512) COLLATE pg_catalog."default",
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE public.artist
(
    id integer NOT NULL DEFAULT nextval('artist_id_seq'::regclass),
    name character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT artist_pkey PRIMARY KEY (id)
);

CREATE TABLE public.song
(
    id integer NOT NULL DEFAULT nextval('song_id_seq'::regclass),
    name character varying(128) COLLATE pg_catalog."default",
    id_artist integer NOT NULL,
    yt_link character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT song_pkey PRIMARY KEY (id),
    CONSTRAINT id_artist_fk FOREIGN KEY (id_artist)
        REFERENCES public.artist (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.playlist
(
    id integer NOT NULL DEFAULT nextval('playlist_id_seq'::regclass),
    name character varying(128) COLLATE pg_catalog."default",
    id_user integer NOT NULL,
    CONSTRAINT playlist_pkey PRIMARY KEY (id),
    CONSTRAINT id_user_fk FOREIGN KEY (id_user)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

create table public.song_list(
	id serial primary key,
	id_playlist int not null,
	id_song int not null,
	constraint id_playlist_fk foreign key(id_playlist) references playlist(id),
	constraint id_song_fk foreign key(id_song) references song(id)
);